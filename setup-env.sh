#!/bin/bash

# This script assumes Ubuntu distro

sudo apt-get update
sudo apt-get install -y vim git python-pip build-essential cscope ctags

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ./vimrc ~/.vimrc
cp ./bashrc ~/.bashrc
vim +PluginInstall +qall
