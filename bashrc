# Default editor
export EDITOR=/usr/bin/vim

# Add colors to the terminal
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'
alias ll='ls -FGlAhp'
alias less='less -FRXc'
alias qfind="find . -name "
alias ..='cd ../'                           # Go back 1 directory level
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels
alias c='clear'                             # c:            Clear terminal display
alias path='echo -e ${PATH//:/\\n}'         # path:         Echo all executable Paths
alias gs='git status'
alias gl='git log'
alias gd='git diff'
alias gb='git branch'
mcd () { mkdir -p "$1" && cd "$1"; }        # mcd:          Makes new Dir and jumps inside
